import { Component, createRef, RefObject } from 'react';
import { MapContainer, TileLayer, useMap } from 'react-leaflet';
import 'leaflet/dist/leaflet.css';
import L from 'leaflet';
import markerIcon from 'leaflet/dist/images/marker-icon.png';
import markerShadow from 'leaflet/dist/images/marker-shadow.png';
import markerRetinaIcon from 'leaflet/dist/images/marker-icon-2x.png';

interface MyMapComponentProps {
  addMap: (map: L.Map) => void;
}

function MyMapComponent({ addMap }: MyMapComponentProps) {
  const map = useMap();
  addMap && addMap(map);
  return null;
}

interface MapProps {
  latitude?: number;
  longitude?: number;
  zoom?: number;
  readOnly?: boolean;
  onChange?: (latitude: number, longitude: number) => void;
}

interface MapState {
  zoom: number;
  latitude: number;
  longitude: number;
  width: number;
  height: number;
}

class Map extends Component<MapProps, MapState> {
  private divRef: RefObject<HTMLDivElement>;
  private map: L.Map | null = null;
  private marker: L.Marker | null = null;
  private latLongTimer: NodeJS.Timeout | null = null;

  static defaultProps: MapProps = {
    latitude: 51.505,
    longitude: -0.09,
    zoom: 14,
    readOnly: false,
    onChange: () => {},
  };

  constructor(props: MapProps) {
    super(props);
    this.state = {
      zoom: props.zoom || 14,
      latitude: props.latitude || 51.505,
      longitude: props.longitude || -0.09,
      width: 0,
      height: 0,
    };
    this.divRef = createRef();
  }

  onMap = (map: L.Map) => {
    if (!this.map || this.map !== map) {
      this.map = map;
      const center: [number, number] = [this.state.latitude, this.state.longitude];
      const customIcon = L.icon({
        iconUrl: markerIcon,
        iconRetinaUrl: markerRetinaIcon,
        shadowUrl: markerShadow,
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        tooltipAnchor: [16, -28],
        shadowSize: [41, 41],
      });

      this.marker = L.marker(center, {
        draggable: true,
        title: 'Resource location',
        alt: 'Resource Location',
        riseOnHover: true,
        icon: customIcon,
      })
        .addTo(map)
        .bindPopup('Popup for any custom information.')
        .on({ dragend: this.onMarkerDragend });
    }
  };

  componentDidUpdate(prevProps: MapProps) {
    if (
      this.map &&
      this.marker &&
      (prevProps.latitude !== this.props.latitude || prevProps.longitude !== this.props.longitude)
    ) {
      this.setState({ latitude: this.props.latitude!, longitude: this.props.longitude! }, () => {
        this.latLongTimer && clearTimeout(this.latLongTimer);
        this.latLongTimer = setTimeout(() => {
          this.latLongTimer = null;
          this.map!.flyTo([this.state.latitude, this.state.longitude]);
          this.marker!.setLatLng([this.state.latitude, this.state.longitude]);
        }, 500);
      });
    }

    if (
      this.divRef.current &&
      (this.state.width !== this.divRef.current.clientWidth || this.state.height !== this.divRef.current.clientHeight)
    ) {
      setTimeout(() => {
        this.setState({ width: this.divRef.current!.clientWidth, height: this.divRef.current!.clientHeight });
      }, 100);
    }
  }

  onMarkerDragend = (evt: L.LeafletEvent) => {
    if (this.props.readOnly) {
      this.map!.flyTo([this.state.latitude, this.state.longitude]);
      this.marker!.setLatLng([this.state.latitude, this.state.longitude]);
      return;
    }
    const ll = (evt.target as L.Marker).getLatLng();
    this.setState({ latitude: ll.lat, longitude: ll.lng }, () =>
      this.props.onChange!(this.state.latitude, this.state.longitude),
    );
  };

  render() {
    const center: [number, number] = [this.state.latitude, this.state.longitude];
    const { zoom } = this.state;

    return (
      <div style={{ width: '100%', height: '100%', minHeight: 350 }} ref={this.divRef}>
        <MapContainer
          style={{
            width: '100%',
            height: '100%',
            minHeight: 350,
            borderRadius: 5,
          }}
          center={center}
          zoom={zoom}
          maxZoom={18}
          attributionControl={false}
          zoomControl
          doubleClickZoom
          scrollWheelZoom={!this.props.readOnly}
          dragging={!this.props.readOnly}
          animate
          easeLinearity={0.35}
        >
          <TileLayer url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
          <MyMapComponent addMap={this.onMap} />
        </MapContainer>
      </div>
    );
  }
}

export default Map;
