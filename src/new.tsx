"use client";

import { useRef, useState } from "react";
import Image from "next/image";
import { usePathname } from "next/navigation";
import { twMerge } from "tailwind-merge";

import SettingsIcon from "@/assets/icons/menu-setting.svg";
import profilePic from "@/assets/images/profile-pic.jpg";
import Logo from "@/assets/logos/logo.png";
import IconArrowDown from "@/assets/newIcons/sidebar-arrow-down.svg";
import IconArrowUp from "@/assets/newIcons/sidebar-arrow-up.svg";
import Button from "@/components/reusable/button";
import Tooltip from "@/components/reusable/tooltip";
import { useOutsideClick } from "@/hooks/useOutsideClick";
import { Link, useRouter } from "@/lib/router-events";
import { coachMenu, menu } from "./constants";
function Navbar() {
  const [openSubmenu, setSubmenu] = useState<
    null | (typeof menu)[number]["subMenu"]
  >(null);

  const ref = useRef<HTMLElement | null>(null);

  const router = useRouter();
  const pathname = usePathname();

  const handleSubClick = (href: string) => {
    router.push(href);
    setSubmenu(null);
  };

  useOutsideClick(ref, () => {
    setSubmenu(null);
  });

  const coach = true;

  const CoachNavbar = () => {
    return (
      <aside
        className="fixed left-0 top-0 flex h-full max-h-screen w-full max-w-[216px] bg-white
          duration-transition"
      >
        <nav className="z-[1] flex w-full flex-col gap-10 px-0 pt-[45px]">
          <Link href="/">
            <Image alt="Logo" height={40} src={Logo.src} width={30} />
          </Link>
          <ul className="flex w-full flex-col gap-1 overflow-y-auto">
            {coachMenu.map(({ url, text, icon: MenuIcon, subMenu, name }) => {
              const isActive = !!(url && pathname.includes(url));
              return (
                <li className="mb-4" key={name}>
                  <Button
                    active={isActive}
                    className={twMerge(
                      `group/menu-link tracking-tight px-4 flex w-full cursor-pointer justify-start
                      rounded-none bg-inherit text-gray-800 font-bold duration-transition
                      hover:bg-sky-50`,
                      isActive && "bg-sky-50 text-blue100 stroke-blue50",
                    )}
                    href={url}
                    onClick={() =>
                      setSubmenu((prev) =>
                        prev && prev.title === text ? null : subMenu,
                      )
                    }
                  >
                    <MenuIcon className="group-hover/menu-link:stroke-blue100 duration-transition" />
                    <span className="block ml-2 group-hover/menu-link:text-blue100 duration-transition">
                      {text}
                    </span>
                    {!!subMenu?.links?.length && (
                      <div className="ml-auto">
                        {openSubmenu && openSubmenu.title === text ? (
                          <IconArrowUp className="group-hover/menu-link:stroke-blue100 duration-transition" />
                        ) : (
                          <IconArrowDown className="group-hover/menu-link:stroke-blue100 duration-transition" />
                        )}
                      </div>
                    )}
                  </Button>
                  {openSubmenu && openSubmenu.title === text && (
                    <>
                      <ul>
                        {openSubmenu.links.map(
                          ({ icon: SubIcon, text, url }) => {
                            const isActive = !!pathname.includes(url);
                            return (
                              <li key={url}>
                                <Button
                                  active={isActive}
                                  className={twMerge(
                                    `group/menu-link px-6 tracking-tight flex w-full cursor-pointer justify-start
                                    rounded-none bg-inherit text-gray-800 duration-transition hover:bg-sky-50`,
                                    isActive &&
                                      "bg-sky-50 text-blue100 stroke-blue100",
                                  )}
                                  onClick={() => handleSubClick(url)}
                                >
                                  <SubIcon className="group-hover/menu-link:stroke-blue100 duration-transition" />
                                  <span className="block ml-2 group-hover/menu-link:text-blue100 duration-transition">
                                    {text}
                                  </span>
                                </Button>
                              </li>
                            );
                          },
                        )}
                      </ul>
                    </>
                  )}
                </li>
              );
            })}
          </ul>
        </nav>
      </aside>
    );
  };
  const DefaultNavBar = () => {
    return (
      <>
        <aside
          className="fixed left-0 top-0 flex h-full max-h-screen w-full max-w-[70px]
            duration-transition"
        >
          <nav
            className="z-[1] flex h-screen max-h-screen w-full flex-col items-center gap-10 bg-gray-100
              px-0 pb-14 pt-[45px]"
          >
            <Link href="/">
              <Image alt="Logo" height={40} src={Logo.src} width={30} />
            </Link>
            <ul className="flex w-full flex-col gap-1 px-3">
              {menu.map(({ url, text, icon: MenuIcon, subMenu, name }) => (
                <li key={name}>
                  <Tooltip isRight text={text}>
                    <Button
                      className="group/menu-link m-0 flex h-11 w-full cursor-pointer items-center justify-center
                        rounded-xl text-gray-800 duration-transition hover:bg-white hover:text-blue100"
                      href={url}
                      onClick={() => setSubmenu(subMenu)}
                    >
                      <MenuIcon />
                    </Button>
                  </Tooltip>
                </li>
              ))}
            </ul>
            <div className="py-0; mt-auto px-3">
              <Link
                className="mb-1.5 flex h-11 w-11 min-w-[44px] cursor-pointer items-center justify-center
                  rounded-xl text-gray-800 duration-transition"
                href="/settings"
              >
                <SettingsIcon height={20} width={20} />
              </Link>
              <Image
                alt="profile picture"
                className="mx-auto rounded-full"
                height={40}
                priority
                quality={100}
                src={profilePic.src}
                width={40}
              />
            </div>
          </nav>
        </aside>
        <aside
          className={twMerge(
            `fixed left-[68px] bg-[#F4F4F4] h-full max-h-screen pb-14 pt-[45px]
            transition-all duration-transition z-20 top-0`,
            openSubmenu ? "visible w-[230px]" : "invisible w-0",
          )}
          ref={ref}
        >
          {openSubmenu && (
            <>
              <h5 className="font-semibold mb-3 pl-3">{openSubmenu.title}</h5>
              <ul>
                {openSubmenu.links.map(({ icon: SubIcon, text, url }) => (
                  <li className="py-2 px-3" key={url}>
                    <Button
                      className="flex items-center gap-2"
                      onClick={() => handleSubClick(url)}
                    >
                      <SubIcon />
                      <span>{text}</span>
                    </Button>
                  </li>
                ))}
              </ul>
            </>
          )}
        </aside>
      </>
    );
  };
  return <>{coach ? <CoachNavbar /> : <DefaultNavBar />}</>;
}

export default Navbar;
import IconAutomations from "@/assets/icons/automations.svg";
import IconClients from "@/assets/icons/menu-clients.svg";
import IconDashboard from "@/assets/icons/menu-dashboard.svg";
import IconDays from "@/assets/icons/menu-days.svg";
import IconFood from "@/assets/icons/menu-food.svg";
import IconForms from "@/assets/icons/menu-forms.svg";
import IconMed from "@/assets/icons/menu-med.svg";
import IconNutrition from "@/assets/icons/menu-nutrition.svg";
import IconPlan from "@/assets/icons/menu-plans.svg";
import IconTeamMembers from "@/assets/icons/menu-team-members.svg";
import IconVault from "@/assets/icons/menu-vault.svg";
import IconWallet from "@/assets/icons/menu-wallet.svg";
import IconWorkout from "@/assets/icons/menu-workout.svg";
import IconCoachAutomations from "@/assets/newIcons/sidebar-automations.svg";
import IconCoachCalendar from "@/assets/newIcons/sidebar-calendar.svg";
import IconCoachClients from "@/assets/newIcons/sidebar-clients.svg";
import IconCoachClientsAll from "@/assets/newIcons/sidebar-clients-all.svg";
import IconCoachCheckIns from "@/assets/newIcons/sidebar-clients-check-ins.svg";
import IconCoachClientsDailyCheckIn from "@/assets/newIcons/sidebar-clients-daily-check-in.svg";
import IconCoachClientsLeads from "@/assets/newIcons/sidebar-clients-leads.svg";
import IconCoachClientsNew from "@/assets/newIcons/sidebar-clients-new.svg";
import IconCoachDashboard from "@/assets/newIcons/sidebar-dashboard.svg";
import IconCoachDays from "@/assets/newIcons/sidebar-days.svg";
import IconCoachFood from "@/assets/newIcons/sidebar-food.svg";
import IconCoachForms from "@/assets/newIcons/sidebar-forms.svg";
import IconCoachFormsContactForm from "@/assets/newIcons/sidebar-forms-contact-form.svg";
import IconCoachFormsDailyHabits from "@/assets/newIcons/sidebar-forms-daily-habits.svg";
import IconCoachFormsInitialQuestionnaire from "@/assets/newIcons/sidebar-forms-initial-questionnaire.svg";
import IconCoachFormsTerms from "@/assets/newIcons/sidebar-forms-terms-conditions.svg";
import IconCoachMeals from "@/assets/newIcons/sidebar-meals.svg";
import IconCoachNutrition from "@/assets/newIcons/sidebar-nutrition.svg";
import IconCoachPackages from "@/assets/newIcons/sidebar-packages.svg";
import IconCoachPlans from "@/assets/newIcons/sidebar-plans.svg";
import IconCoachRequests from "@/assets/newIcons/sidebar-requests.svg";
import IconCoachTeamMembers from "@/assets/newIcons/sidebar-team-members.svg";
import IconCoachVault from "@/assets/newIcons/sidebar-vault.svg";
import IconCoachWorkouts from "@/assets/newIcons/sidebar-workouts.svg";
import IconCoachCircuits from "@/assets/newIcons/sidebar-workouts-circuits.svg";
import IconCoachExercises from "@/assets/newIcons/sidebar-workouts-exercises.svg";

export enum RouteName {
  NutritionPlans = "nutrition-plans",
  NutritionPlan = "nutrition-plan",
  NutritionPlanCreate = "create-nutrition-plan",
  NutritionPlanDays = "nutrition-days",
  NutritionPlanMeals = "nutrition-meals",
  NutritionPlanMealTemplate = "nutrition-meal-template",
  Calendar = "calendar",
  Dashboard = "dashboard",
  AllClients = "all-clients",
  NewClients = "new-clients",
  CheckInsClients = "check-ins-clients",
  DailyCheckInClients = "daily-check-in-clients",
  Leads = "leads",
  TeamMembers = "team-members",
  Packages = "packages",
  NutritionPlanFood = "nutrition-plan-meals",
  WorkoutPlans = "workout-plans",
  WorkoutCircuits = "workout-circuits",
  WorkoutExercises = "workout-exercises",
  ExerciseLibrary = "exercise-library",
  SupplementPlans = "supplement-plans",
  InitialQuestionnaire = "initial-questionnaire",
  CheckInForm = "check-in-form",
  DailyProgressHabits = "daily-progress-habits",
  ContactForm = "contact-form",
  TermsConditions = "terms-conditions",
  Vault = "vault",
  Automations = "automations",
  Settings = "settings",
  FeatureRequests = "requests",
}

enum MenuName {
  Dashboard = RouteName.Dashboard,
  Clients = "clients",
  ClientsSubMenu = "clients-sub-menu",
  AllClients = RouteName.AllClients,
  NewClients = RouteName.NewClients,
  CheckInsClients = RouteName.CheckInsClients,
  DailyCheckInClients = RouteName.DailyCheckInClients,
  Leads = RouteName.Leads,
  TeamMembers = RouteName.TeamMembers,
  Packages = RouteName.Packages,
  Nutrition = "nutrition",
  NutritionSubMenu = "nutrition-sub-menu",
  NutritionPlans = RouteName.NutritionPlans,
  NutritionPlanDays = RouteName.NutritionPlanDays,
  NutritionPlanMeals = RouteName.NutritionPlanMeals,
  NutritionPlanFood = RouteName.NutritionPlanFood,
  Workouts = "workouts",
  WorkoutPlansSubMenu = "workout-plans-sub-menu",
  WorkoutPlans = RouteName.WorkoutPlans,
  WorkoutCircuits = RouteName.WorkoutCircuits,
  WorkoutExercises = RouteName.WorkoutExercises,
  ExerciseLibrary = RouteName.ExerciseLibrary,
  Calendar = RouteName.Calendar,
  SupplementPlans = RouteName.SupplementPlans,
  Forms = "forms",
  FormsSubMenu = "forms-sub-menu",
  InitialQuestionnaire = RouteName.InitialQuestionnaire,
  CheckInForm = RouteName.CheckInForm,
  DailyProgressHabits = RouteName.DailyProgressHabits,
  ContactForm = RouteName.ContactForm,
  TermsConditions = RouteName.TermsConditions,
  Vault = RouteName.Vault,
  Automations = RouteName.Automations,
  FeatureRequests = RouteName.FeatureRequests,
}
export const menu = [
  {
    text: "Dashboard",
    name: MenuName.Dashboard,
    url: "/dashboard",
    icon: IconDashboard,
  },
  {
    text: "Clients",
    name: MenuName.Clients,
    url: "",
    icon: IconClients,
    subMenu: {
      title: "Clients",
      name: MenuName.ClientsSubMenu,
      links: [
        {
          icon: IconClients,
          text: "All",
          name: MenuName.AllClients,
          label: "All clients",
          url: "/coach/clients",
        },
        {
          icon: IconDays,
          text: "New",
          name: MenuName.NewClients,
          label: "New clients",
          url: "/coach/clients/new",
        },
        {
          icon: IconNutrition,
          text: "Check ins",
          name: MenuName.CheckInsClients,
          label: "Check ins clients",
          url: "/coach/clients/check_ins",
        },
        {
          icon: IconFood,
          text: "Leads",
          name: MenuName.Leads,
          label: "Leads",
          url: "/coach/coach_form_view",
        },
      ],
    },
  },
  {
    text: "Team Members",
    name: MenuName.TeamMembers,
    url: "/team",
    icon: IconTeamMembers,
  },
  {
    text: "Packeges",
    name: MenuName.Packages,
    url: "/coach/packages",
    icon: IconWallet,
  },
  {
    text: "Nutrition Plans",
    url: "",
    name: MenuName.Nutrition,
    icon: IconNutrition,
    subMenu: {
      title: "Nutrition",
      name: MenuName.NutritionSubMenu,
      links: [
        {
          icon: IconPlan,
          text: "Plans",
          name: MenuName.NutritionPlans,
          label: "Nutrition Plans",
          url: "/coach/diet_plans",
        },
        {
          icon: IconDays,
          text: "Days",
          name: MenuName.NutritionPlanDays,
          label: "Foods List",
          url: "/coach/diet_plan_days",
        },
        {
          icon: IconNutrition,
          text: "Meals",
          name: MenuName.NutritionPlanMeals,
          label: "Foods List",
          url: "/coach/diet_plan_meals",
        },
        {
          icon: IconFood,
          text: "Food",
          name: MenuName.NutritionPlanFood,
          label: "Foods List",
          url: "/coach/foods",
        },
      ],
    },
  },
  {
    text: "Workout Plans",
    url: "",
    name: MenuName.Workouts,
    icon: IconWorkout,
    subMenu: {
      title: "Workouts",
      name: MenuName.WorkoutPlansSubMenu,
      links: [
        {
          icon: IconPlan,
          text: "Plans",
          name: MenuName.WorkoutPlans,
          label: "Workout plans",
          url: "/coach/workout_plans",
        },
        {
          icon: IconDays,
          text: "Exercise Library",
          name: MenuName.ExerciseLibrary,
          label: "Exercise Library",
          url: "/coach/exercises",
        },
      ],
    },
  },
  {
    text: "Calendar",
    url: "/coach/calendar",
    name: RouteName.Calendar,
    icon: IconDays,
  },
  {
    text: "Supplement plans",
    url: "/coach/supplement_plans",
    name: MenuName.SupplementPlans,
    icon: IconMed,
  },
  {
    text: "Forms",
    url: "",
    name: MenuName.Forms,
    icon: IconForms,
    subMenu: {
      title: "Forms",
      name: MenuName.FormsSubMenu,
      links: [
        {
          icon: IconForms,
          text: "Initial Questionnaire",
          name: MenuName.InitialQuestionnaire,
          label: "Initial Questionnaire",
          url: "/coach/initial_questionnaire",
        },
        {
          icon: IconForms,
          text: "Check in form",
          name: MenuName.CheckInForm,
          label: "Check in form",
          url: "/coach/check_in_form",
        },
        {
          icon: IconForms,
          text: "Daily Progress & Habits",
          name: MenuName.DailyProgressHabits,
          label: "Daily Progress & Habits",
          url: "/coach/daily_habits",
        },
        {
          icon: IconForms,
          text: "Contact Form",
          name: MenuName.ContactForm,
          label: "Contact Form",
          url: "/coach/coach_form_list",
        },
        {
          icon: IconForms,
          text: "Terms & Conditions",
          name: MenuName.TermsConditions,
          label: "Terms & Conditions",
          url: "/coach/document_terms",
        },
      ],
    },
  },
  {
    text: "Vault",
    name: RouteName.Vault,
    url: "/vault/files",
    icon: IconVault,
  },
  {
    text: "Automations",
    name: MenuName.Automations,
    url: "/automations",
    icon: IconAutomations,
  },
];
export const coachMenu = [
  {
    text: "Dashboard",
    name: MenuName.Dashboard,
    url: "/dashboard",
    icon: IconCoachDashboard,
  },
  {
    text: "Clients",
    name: MenuName.Clients,
    url: "",
    icon: IconCoachClients,
    subMenu: {
      title: "Clients",
      name: MenuName.ClientsSubMenu,
      links: [
        {
          icon: IconCoachClientsAll,
          text: "All",
          name: MenuName.AllClients,
          label: "All clients",
          url: "/coach/clients",
        },
        {
          icon: IconCoachClientsNew,
          text: "New",
          name: MenuName.NewClients,
          label: "New clients",
          url: "/coach/clients/new",
        },
        {
          icon: IconCoachCheckIns,
          text: "Check ins",
          name: MenuName.CheckInsClients,
          label: "Check ins clients",
          url: "/coach/clients/check_ins",
        },
        {
          icon: IconCoachClientsDailyCheckIn,
          text: "Daily Check-in",
          name: MenuName.DailyCheckInClients,
          label: "Daily check in clients",
          url: "/coach/clients/daily_check_in",
        },
        {
          icon: IconCoachClientsLeads,
          text: "Leads",
          name: MenuName.Leads,
          label: "Leads",
          url: "/coach/coach_form_view",
        },
      ],
    },
  },
  {
    text: "Team Members",
    name: MenuName.TeamMembers,
    url: "/team",
    icon: IconCoachTeamMembers,
  },
  {
    text: "Packages",
    name: MenuName.Packages,
    url: "/coach/packages",
    icon: IconCoachPackages,
  },
  {
    text: "Nutrition",
    url: "",
    name: MenuName.Nutrition,
    icon: IconCoachNutrition,
    subMenu: {
      title: "Nutrition",
      name: MenuName.NutritionSubMenu,
      links: [
        {
          icon: IconCoachPlans,
          text: "Plans",
          name: MenuName.NutritionPlans,
          label: "Nutrition Plans",
          url: "/coach/diet_plans",
        },
        {
          icon: IconCoachDays,
          text: "Days",
          name: MenuName.NutritionPlanDays,
          label: "Foods List",
          url: "/coach/diet_plan_days",
        },
        {
          icon: IconCoachMeals,
          text: "Meals",
          name: MenuName.NutritionPlanMeals,
          label: "Foods List",
          url: "/coach/diet_plan_meals",
        },
        {
          icon: IconCoachFood,
          text: "Food",
          name: MenuName.NutritionPlanFood,
          label: "Foods List",
          url: "/coach/foods",
        },
      ],
    },
  },
  {
    text: "Workouts",
    url: "",
    name: MenuName.Workouts,
    icon: IconCoachWorkouts,
    subMenu: {
      title: "Workouts",
      name: MenuName.WorkoutPlansSubMenu,
      links: [
        {
          icon: IconCoachPlans,
          text: "Programs",
          name: MenuName.WorkoutPlans,
          label: "Workout programs",
          url: "/coach/workout_programs",
        },
        {
          icon: IconCoachDays,
          text: "Workouts",
          name: MenuName.Workouts,
          label: "Workouts",
          url: "/coach/workouts",
        },
        {
          icon: IconCoachCircuits,
          text: "Circuits",
          name: MenuName.WorkoutCircuits,
          label: "Circuits",
          url: "/coach/circuits",
        },
        {
          icon: IconCoachExercises,
          text: "Exercises",
          name: MenuName.WorkoutExercises,
          label: "Exercises",
          url: "/coach/exercises",
        },
      ],
    },
  },
  {
    text: "Calendar",
    url: "/coach/calendars",
    name: RouteName.Calendar,
    icon: IconCoachCalendar,
  },
  {
    text: "Supplement Plans",
    url: "/coach/supplement_plans",
    name: MenuName.SupplementPlans,
    icon: IconCoachPlans,
  },
  {
    text: "Forms",
    url: "",
    name: MenuName.Forms,
    icon: IconCoachForms,
    subMenu: {
      title: "Forms",
      name: MenuName.FormsSubMenu,
      links: [
        {
          icon: IconCoachFormsInitialQuestionnaire,
          text: "Initial Questionnaire",
          name: MenuName.InitialQuestionnaire,
          label: "Initial Questionnaire",
          url: "/coach/initial_questionnaire",
        },
        {
          icon: IconCoachCheckIns,
          text: "Check-in Form",
          name: MenuName.CheckInForm,
          label: "Check-in Form",
          url: "/coach/check_in_form",
        },
        {
          icon: IconCoachFormsDailyHabits,
          text: "Daily Habits",
          name: MenuName.DailyProgressHabits,
          label: "Daily Habits",
          url: "/coach/daily_habits",
        },
        {
          icon: IconCoachFormsContactForm,
          text: "Contact Form",
          name: MenuName.ContactForm,
          label: "Contact Form",
          url: "/coach/coach_form_list",
        },
        {
          icon: IconCoachFormsTerms,
          text: "Terms & Conditions",
          name: MenuName.TermsConditions,
          label: "Terms & Conditions",
          url: "/coach/document_terms",
        },
      ],
    },
  },
  {
    text: "Vault",
    name: RouteName.Vault,
    url: "/vault/manage",
    icon: IconCoachVault,
  },
  {
    text: "Automations",
    name: MenuName.Automations,
    url: "/admin/trigger_list",
    icon: IconCoachAutomations,
  },
  {
    text: "Feature Requests",
    name: MenuName.FeatureRequests,
    url: "https://app.gleap.io/sharedboard/c2ebBZti0bD0DIy7k3y5sVIhchvuIS43",
    icon: IconCoachRequests,
  },
];
